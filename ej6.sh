#! /bin/bash

#6. Escribir un guión que acepte dos parámetros y que renombre en bloque a aquellos ficheros
#que contengan el primer parámetro en su nombre, sustituyéndolo por el segundo. (1 punto).

# Parametros introducidos
a=$1 # fichero con el primer parametro en su nombre
b=$2 # segun parametro que sustituira al primero en el nombre

if [ $# == 2 ];
	then
      mv $a* $b
else
      echo "El numero de argumentos no es válido \n"
fi