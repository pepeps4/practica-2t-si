#!/bin/bash

#3. Escribir un guión que acepte un parámetro y que muestre aquellos directorios que estén
#comprendidos dentro del directorio actual o sus subdirectorios que contengan el parámetro en
#su nombre y que permita escritura a todos los grupos de usuarios (1 punto).

# Parametros introducidos
a=$1 # fichero a buscar

if [ $# == 1 ];
	then
	  chmod ug+w $a*
      ls -l $a*
else
      echo "El numero de argumentos no es válido \n"
fi