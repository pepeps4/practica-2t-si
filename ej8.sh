#!/bin/bash

#8. Escribir un guión que acepte al menos dos parámetros. El primero será el nombre de un
#fichero y el siguiente o siguientes serán nombres de directorios. El guión mostrará los detalles
#de los ficheros que contengan el nombre del parámetro 1 y que se encuentren en los
#directorios especificados (1 punto)

# Parametros introducidos
a=$1 # fichero a buscar
b=$2 # directorio en el que se buscará

if [ $# == 2 ];
	then
      find $b -type f -name $a*
else
      echo "El numero de argumentos no es válido \n"
fi
