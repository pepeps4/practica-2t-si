#!/bin/bash

#5. Escribir un guión que acepte un parámetro y que borre los directorios vacíos que encuentre
#dentro del directorio especificado como parámetro (1 punto).

# Parametros introducidos
a=$1 # directorio a borrar (vacio)

if [ $# == 1 ];
	then
		find $a -empty -type d -delete
else
      echo "El numero de argumentos no es válido \n"
fi
